######################################
#Creating objects and parameters     #
######################################


## Creating objects which R will store the information or parameters
H<-numeric()
C<-numeric()
VC<-numeric()
Tr<-numeric()
D<-numeric()
Fa<-numeric()
Fa1<-numeric()
flock<-list()
batch<-list()
time_flock<-list()
time_batch<-list()
amu<-list()
amu1<-list()

media_H<-numeric()
media_H2<-numeric()
media_C<-numeric()
media_C2<-numeric()
media_F<-numeric()
media_F1<-numeric()
media_F2<-numeric()
media_F3<-numeric()
media_VC<-numeric()
media_VC1<-numeric()


r1<-numeric()
r2<-numeric()
r2.1<-numeric()
r3<-numeric()
r3.1<-numeric()
r4<-numeric()
r4.1<-numeric()
r5<-numeric()
r6<-numeric()
r6.1<-numeric()
r6.2<-numeric()
r6.3<-numeric()
r7<-numeric()
r7.1<-numeric()
r8<-numeric()
r8.1<-numeric()
r9<-list()
ere9<-numeric()
r10<-list()
ere10<-numeric()
ere9.1<-numeric()
ere10.1<-numeric()
r11<-list()
r12<-list()
r13<-list()
r14<-list()
r14<-list()



p<-numeric()
p1<-numeric()
p2<-numeric()
p3<-numeric()
p4<-numeric()
p3.1<-numeric()
p4.1<-numeric()
p5<-list()
p6<-list()
p7<-list()
p8<-list()
media<-numeric()
media1<-numeric()

Nenv1<-numeric()
Next1<-numeric()
Nenv2<-numeric()
Next2<-numeric()
Nenv3<-numeric()
Next3<-numeric()
Nenv4<-numeric()
Next4<-numeric()
Nenv5<-numeric()
Next5<-numeric()
Nenv_beef1<-numeric()
Next_beef1<-numeric()
Next_beef5<-numeric()
a<-numeric()
a1<-numeric()
prev_slaug<-numeric()
prev_slaug_beef<-numeric()
prev_cons<-numeric()
prev_chi<-numeric()
prev_beef<-numeric()
prev_veg<-numeric()
prev_veg1<-numeric()
raw<-numeric()
raw1<-numeric()
raw_af<-numeric()
raw_af1<-numeric()
raw_cross<-numeric()
raw_cross1<-numeric()
veg_cross<-numeric()
veg_cross1<-numeric()
ready<-numeric()
ready1<-numeric()
ready2<-numeric()
ready2.1<-numeric()
dose_chi<-numeric()
dose_beef<-numeric()
dose_veg<-numeric()
dose_veg1<-numeric()


OC<-list()
FA<-list()
Ce<-list()
ME<-list()

media_H2<-numeric()  
media_C2<-numeric()
media_F2<-numeric()



R1<-list()
R2<-list()
R3<-list()
R4<-list()
R6<-list()
R6.1<-list()
R7<-list()
ERE9<-list()
ERE10<-list()


A<-list()
Prev_slaug<-list()
Raw_af<-list()
Raw<-list()
Veg_cross<-list()
Raw_cross<-list()
Ready<-list()
Ready2<-list()
Dose_chi<-list()
Dose_veg<-list()





## Infection dynamics in Open commmunity

prev1<-0.05                                                                               #prev in human time zero
col_proc<- c(1/27,1/27,1/27,1/27,1/27,1/27,1/27,1/27,
             1/57,1/12,1/27,1/27,1/27,1/27,1/27,1/27,
             1/27,1/27,1/27,1/27,1/27,1/27,1/27,1/27,1/27,1/27,1/27,1/27,1/27)                                               #rate of within-household colonization (week)  Haverkate et al. (2017) 

fade_h<-c(1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,
          1/16,1/31,1/8,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16)                # Rate of colonization clearance in healthy people Haverkate et al. (2017)


#beta(154,10625)=(0.012; 0.014; 0.016)/4
expo_fa<-c(0.003645,0.003092674,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645
,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.0042,0.003645,0.003645,0.003645,0.003645)*(1800/1800)   #Proportion of humans in open community which have contact with BF farmers Mughini-Gras et al. (2019) Supp appendix S2.

#beta(154,10625)=(0.012; 0.014; 0.016)/4
expo_fa1<-c(0.003645,0.003092674,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645
            ,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.003645,0.0042,0.003645,0.003645,0.003645,0.003645)*(1300/1800)   #Proportion of humans in open community which have contact with VC farmers Mughini-Gras et al. (2019)


## Infection dynamics in farmers community
prev2<-0.05                                                                               #prev in chicken farmers time zero

#beta(6+1,18-6+1)/5.1=(0.12; 0.19; 0.19)
rate1<-c(0.19,0.19,0.19,0.19,0.19,0.19,0.19,0.19,0.19,0.19,0.19,0.19,0.19,0.19
         ,0.12,0.29,0.19,0.19,0.19,0.19,0.19,0.19,0.19,0.19,0.19,0.19,0.19,0.19,0.19)     #Prob of colonization of farmer given contact with a flock Huijbers 2014

col_proc2<- 1-exp(-1/27)                                                                  #Colonization given the contact with open community Haverkate et al. (2017) 

fade_fa<-c(1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,
           1/16,1/31,1/8,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16,1/16)#average time (week) to colonization cleareance in healthy people Haverkate et al. (2017)



prev2.1<-0.05                                                                              #prev in veal farmers time zero
rate1.1<-c(0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1
          ,0.05,0.2,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1)                  #Prob of colonization of farmer given contact with calves       

col_proc2.1<- 1-exp(-1/27)                                                                 #Colonization given the contact with open community 
fade_fa1<-c(16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16,16)^-1




## Infection dynamics in farm animals

prev3<-0.01                                          #prev in chicken time zero
prev4<-0.25                                           #prev in vc time zero

beta_ch<-c(0.7, 0.7, 0.56, 0.7, 0.7, 0.7, 0.7,0.7,0.7,0.7,0.7,
           0.7, 0.56, 0.84 ,0.7, 0.7, 0.7, 0.7, 0.7, 0.7,0.7, 0.7, 0.7, 0.7,0.7,0.7,0.7,0.7,0.7)                          #Beta direct flocks Dama-Koerevaar et al. thesis pg. 86. parameter value guess to fit the model


beta_vc<-c(0.113, 0.113, 0.12, 0.113, 0.113, 0.113, 0.113,0.113,0.113,0.113,0.113,
           0.113,0.1,0.113,0.113,0.113, 0.113, 0.113, 0.113,0.113,0.0113,0.113,0.113,0.113,0.113,0.113,0.113,0.113,0.113)                 #Beta rate (calves/week) direct vc longitudinal vc study



fade_ch<-c(0.6,0.6,0.72,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6,
           0.6,0.6,0.6,0.6,0.6,0.48,0.72,0.6,0.6,0.6,0.6,0.6,0.6,0.6,0.6)                                                            #rate (ch/weeks) of decolonization. Value guess to fit the model


fade_vc<-c(0.077,0.077,0.53,0.077,0.077,0.077,0.077,0.077,0.077,0.077,0.077,0.077,0.077,0.077,
           0.077,0.077,0.077,0.077,0.077,0.077,0.077,0.077,0.077,0.077,0.077)                                                     #rate (calves/week) of decolonization longitudinal vc study



rate2<-c(0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.001,0.02,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01)    #Prob of colonization of a chicken given the contact with a farmer  Guess    



rate2.1<-c(0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.01,0.2,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1)          #Prob of colonization of a calve given the contact with a farmer: Guess    

#rate4<-c(0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.1,0.01,0.2,0.1,0.1,0.1,0.1)          #Prob of colonization of a calve given the contact with OC: Guess    


heavy_prop<-c(0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.015,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,
              0.01,0.0075,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01)                                                                         #proportion of heavy ATM flocks No data, scenario created based on Luiken et al. (2019)

heavy_b<-c(1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.46,1.05,2,1.46,1.46,1.46,1.46,1.46,1.46)            #effect of high ATU on the beta_ch  Luiken et al. (2019) Tb2 and Fig2

heavy_f<-c(0.54,0.54,0.54,0.54,0.54,0.54,0.54,0.54,0.54,0.54,0.54,0.54,0.54
           ,0.54,0.54,0.54,0.54,0.54,0.54,0.54,0.54,0.95,0.01,0.54,0.54,0.54,0.54,0.54,0.54)            #effect of high ATU on the fade_ch  Luiken et al. (2019)



heavy_prop1<-c(0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.1,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,
               0.01,0.001,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01,0.01)                                                                         #proportion of heavy ATM veal farms No data, scenario created based on guess

heavy_b1<-c(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1)   #effect of high ATU on the beta_vc  longitudinal VC study

heavy_f1<-c(1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1)   #effect of high ATU on the fade_vc  longitudinal VC study

## Chicken slaughter parameters
# Scalding paramenters
aext1<-0.053         # tranfer from carcass to environ during scalding
cext1<-0.18          # inactivation/removal from carcass surface during scalding https://browser.combase.cc/ComBase_Predictor.aspx?model=2 & Pacholewicz et al. 2016
benv1<-c(1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,
         1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5,1.1*10^-5)     # transfer from envir to carcass surface during scalding
cenv1<-0.98          # inactivation from environ during scalding https://browser.combase.cc/ComBase_Predictor.aspx?model=2 & Pacholewicz et al. 2016
afec1<-1-(1.4*10^-6) # 1- probability of 1 CFU from the faeces to the carcass
pfec1<-c(0.03,0.03,0.03,0.03,0.03,0.03,0.01,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03)          # probability of fecal leakage during scalding Pacholewicz et al. 2016
wfec1<-c(1.3,1.3,1.3,1.3,1.3,1.3,0.6,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3,1.3)           # amount of feces that leakes from a carfcass (grams) during scalding Nauta et al. (2005)
sig_wfec1<-0.38      # SD amount of feces that leakes from a carfcass (grams) during scalding Nauta et al. (2005)
conc1<-5.68          # concentration o ESBL in feces log10(CFU/gram) Ceccarelli et al. (2017)
sig_conc1<-0.67      # SD concentration o ESBL in feces log10(CFU/gram) Ceccarelli et al. (2017)



# Defeathering
aext2<-0.65          # tranfer from carcass to environ during scalding
cext2<-0             # inactivation/removal from carcass surface during scalding https://browser.combase.cc/ComBase_Predictor.aspx?model=2 & Pacholewicz et al. 2016
benv2<-c(0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02)     # transfer from envir to carcass surface during scalding
cenv2<-0.98          # inactivation from environ during scalding https://browser.combase.cc/ComBase_Predictor.aspx?model=2 & Pacholewicz et al. 2016
afec2<-1-(1.4*10^-6) # 1- probability of 1 CFU from the faeces to the carcass
pfec2<-c(0.03,0.03,0.03,0.03,0.03,0.03,0.01,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03)          # probability of fecal leakage during scalding Pacholewicz et al. 2016
wfec2<-c(4.3,4.3,4.3,4.3,4.3,4.3,3.6,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3)           # amount of feces that leakes from a carfcass (grams) during scalding Nauta et al. (2005)
sig_wfec1<-0.38      # SD amount of feces that leakes from a carfcass (grams) during scalding Nauta et al. (2005)
conc2<-5.68          # concentration o ESBL in feces log10(CFU/gram) Ceccarelli et al. (2017)
sig_conc2<-0.67      # SD concentration o ESBL in feces log10(CFU/gram) Ceccarelli et al. (2017)


# Evisceration
aext3<-0.053         # tranfer from carcass to environ during scalding
cext3<-0.5          # inactivation/removal from carcass surface during scalding https://browser.combase.cc/ComBase_Predictor.aspx?model=2 & Pacholewicz et al. 2016
benv3<-c(0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02)     # transfer from envir to carcass surface during scalding
cenv3<-0.98          # inactivation from environ during scalding https://browser.combase.cc/ComBase_Predictor.aspx?model=2 & Pacholewicz et al. 2016
afec3<-1-0.001 # 1- probability of 1 CFU from the faeces to the carcass
pfec3<-c(0.03,0.03,0.03,0.03,0.03,0.03,0.01,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03,0.03)          # probability of fecal leakage during scalding Pacholewicz et al. 2016
wfec3<-c(4.3,4.3,4.3,4.3,4.3,4.3,3.6,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3)           # amount of feces that leakes from a carfcass (grams) during scalding Nauta et al. (2005)
sig_wfec3<-0.38      # SD amount of feces that leakes from a carfcass (grams) during scalding Nauta et al. (2005)
conc3<-5.68          # concentration o ESBL in feces log10(CFU/gram) Ceccarelli et al. (2017)
sig_conc3<-0.67      # SD concentration o ESBL in feces log10(CFU/gram) Ceccarelli et al. (2017)


# Chilling
aext5<-0.01         # transfer from carcass to environ during scalding
cext5<-0.74          # inactivation/removal from carcass surface during scalding https://browser.combase.cc/ComBase_Predictor.aspx?model=2 & Pacholewicz et al. 2016
benv5<-c(0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02)     # transfer from envir to carcass surface during scalding
cenv5<-0.98          # inactivation from environ during scalding https://browser.combase.cc/ComBase_Predictor.aspx?model=2 & Pacholewicz et al. 2016



# Carcass weight in grams 

car_me<-1.190*1000    # Pacholewicz et al. 2016
car_sd<-0.02*1000
red_carc<-0.9968377 #reduction in % on cfu/mL Pacholewicz 2015




## Veal calves slaughter parameters
# Evisceration parameters
aext_beef1<-0.01       # transfer from carcass to environ during evisceration (guess)
cext_beef1<-0.316          # inactivation/removal from carcass surface during Evisceration Cummins 2008 
benv_beef1<-c(0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02,0.02)  # transfer from envir to carcass surface during skinning Perez-Rodrigues_2007
cenv_beef1<-0.5       # inactivation from environ during evisceration (guess)
afec_beef1<-1-(110/2490)  # 1- probability of fecal contamination during evisceration/manipulation guess
pfec_beef1<-c(0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,
              0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039,0.0039) # probability of fecal leakage during evisceration Cummins 2008 mean(10^-(runif(1000,2,3)))

wfec_beef1<-c(4.3,4.3,4.3,4.3,4.3,4.3,4.3,5,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3,4.3)        # weight of feces that leaks from a carcass (grams) during evisceration
sig_wfec_beef1<-0.38   # SD amount of feces that leaks from a carcass (grams) during evisceration
conc_beef1<-5.68       # concentration o ESBL in feces log10(CFU/gram)  
sig_conc_beef1<-0.67   # SD concentration o ESBL in feces log10(CFU/gram) 


car_beef_me<-380*1000  #Caracas weight in grams (Anita)
car_beef_sd<-15*1000   #Caracas sd in grams (guess)
red_carc1<-0.628 #reduction percentage in cfu/cm2 Yang 2012
area_carc<-32000 #Area of a carcass in cm2 Cummins, 2008



## Parameters at consumer

# Bacterial growth at consumer phase parameters

tgen_min<-0.47                                                                               # min generation time cited by Evers et al. (2016)
Tmin<-6                                                                                      # min temperature growth cited by Evers et al. (2016)
Topt<-37.5                                                                                   # Optimum growth temperature cited by Evers et al. (2016)
Tst<-5.99                                                                                    #Temperature storage in fridge in celcius for meat to be cooked Evers et al. (2016)
tst<-c(3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024,3024) #Time storage in minutes at fridge temperature for meat to be cooked Evers et al. (2016)

# Cross-contamination at consumer

cross<-c(1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3,1.5e-3)      # Fraction of bacteria tranfer to salad Evers et al. (2016)
frac_cross<-c(0.47,0.47,0.47,0.47,0.35,0.58,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47,0.47)                                         #Fraction of meals prepared chicken + vegetables

# Bacterial inactivation at consumer and consumption

#Chicken
Tend<-71.5     # Final heating temp Evers et al. (2016)
Tref<-70       # Temp reference for D value Evers et al. (2016)
the<-15        # time cooking (minutes) Evers et al. (2016)
Dref<-(-0.67)  # log Time variation (minutes) to decrease the E. coli in 90% at 70 C Evers et al. (2016)
z<- 10.6       # Temp variation to decrease D value in 90% Evers et al. (2016)
Dvalue<-10^(Dref-(Tend-Tref)/z)
consu_ch<-c(34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,34.5,25.87,34.5,34.5,34.5,34.5,34.5)       #Chicken consumption per capta in grams/portion https://www.wateetnederland.nl/resultaten/voedingsmiddelen/consumptie/vlees (35% Dutch production)
prob_consu<-c(1,1,1,0.75,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1)                                #Weekly consumption of 1 portion of chicken in NL #RIVM 2017

#Beef
Tend1<-61.5     # Final heating temp Evers et al. (2016)
the1<-10        # time cooking (minutes) Evers et al. (2016) weighted by profile
Dvalue1<-10^(Dref-(Tend1-Tref)/z)
consu_beef<-25*1                                                                       #Beef consumption per capta in grams/portion https://www.agrimatie.nl/SectorResultaat.aspx?subpubID=2232&sectorID=2430&themaID=2904 (Assuming 1% Dutch production)
prob_consu_beef<-c(1,1,1,0.75,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1)                  #Weekly consumption of 1 portion of beef in NL #RIVM 2017


#Vegetables

consu_veg<-600                                                                            #Vegetables consumption per capta in grams/portion https://www.wateetnederland.nl/resultaten/voedingsmiddelen/consumptie/groenten
non_veg<-0.955                                                                            #Frequency of non-vegetarians in NL #RIVM 2017

#Dose response
alfa<-c(713,713,713,713,713,713,713,713,713,713,713,713,713,713,713,713,713,713,713,713,713,713,713,713,713,570,855,713,713)     #Evers
gama<-c(0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.267,0.2136,0.3204)        #Evers
